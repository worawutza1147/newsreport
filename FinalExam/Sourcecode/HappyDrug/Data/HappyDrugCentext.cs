using HappyDrug.Models; 
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore; 


namespace HappyDrug.Data 
{ 
    public class HappyDrugContext : IdentityDbContext<NewUser>    
	{  
		public DbSet<Category> drugList { get; set; }
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=HappyDrug.db");
		}
	}
	
}