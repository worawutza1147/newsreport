using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using HappyDrug.Data;
using HappyDrug.Models;

namespace HappyDrug.Pages.Admin
{
    public class CreateModel : PageModel
    {
        private readonly HappyDrug.Data.HappyDrugContext _context;

        public CreateModel(HappyDrug.Data.HappyDrugContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["NewUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public Category Category { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.drugList.Add(Category);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}