using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using HappyDrug.Data;
using HappyDrug.Models;

namespace HappyDrug.Pages.Admin
{
    public class DetailsModel : PageModel
    {
        private readonly HappyDrug.Data.HappyDrugContext _context;

        public DetailsModel(HappyDrug.Data.HappyDrugContext context)
        {
            _context = context;
        }

        public Category Category { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category = await _context.drugList
                .Include(c => c.checkUser).FirstOrDefaultAsync(m => m.CategoryID == id);

            if (Category == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
