using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using HappyDrug.Data;
using HappyDrug.Models;

namespace HappyDrug.Pages.Admin
{
    public class DeleteModel : PageModel
    {
        private readonly HappyDrug.Data.HappyDrugContext _context;

        public DeleteModel(HappyDrug.Data.HappyDrugContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Category Category { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category = await _context.drugList
                .Include(c => c.checkUser).FirstOrDefaultAsync(m => m.CategoryID == id);

            if (Category == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Category = await _context.drugList.FindAsync(id);

            if (Category != null)
            {
                _context.drugList.Remove(Category);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
