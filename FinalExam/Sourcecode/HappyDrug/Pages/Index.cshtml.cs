﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.EntityFrameworkCore; 
using HappyDrug.Data; 
using HappyDrug.Models;

namespace HappyDrug.Pages
{
    public class IndexModel : PageModel
    {
        private readonly HappyDrug.Data.HappyDrugContext _context;
		
		public IndexModel(HappyDrug.Data.HappyDrugContext context)
		{
			_context = context;
		}
		
		public IList<Category> Drugs { get; set; }
		
		public async Task OnGetAsync()
		{
			Drugs = await _context.drugList.ToListAsync();
		}
    }
}
