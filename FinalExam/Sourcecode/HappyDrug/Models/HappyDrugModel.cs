using System; 
using System.ComponentModel.DataAnnotations; 
using Microsoft.AspNetCore.Identity;

namespace HappyDrug.Models
{
	public class NewUser : IdentityUser
	{
		public string  FirstName { get; set;}         
		public string  LastName { get; set;} 
	}
	public class Category
	{
		public int CategoryID { get; set; }
		public string CatName { get; set; }
		public int Stock { get; set; }
		
		public string NewUserId {get; set;}         
		public NewUser  checkUser {get; set;} 
	}
	
	
}